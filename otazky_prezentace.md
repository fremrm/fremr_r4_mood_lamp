# Otázky na prezentaci
**Zodpovězte prosím pro třídu pravdivě následující otázky**
| otázka                                        | vyjádření                       |
| :-------------------------------------------- | :------------------------------ |
| jak dlouho mi tvorba zabrala - **čistý čas**  |         8hodin                        |
| odkud jsem čerpal inspiraci                   | můj nápad                        |
| odkaz na video                                | https://www.youtube.com/watch?v=rRoo-xkg-6k                     |
| jak se mi to podařilo rozplánovat             |        Ok                         |
| proč jsem zvolil tento design                 |         přisel mi nejklasičtější                        |
| zapojení                                      | https://gitlab.spseplzen.cz/fremrm/fremr_r4_mood_lamp/-/blob/main/dokumentace/schema/MOOD_LAMP_FRITZIG_OBRAZEK.png       |
| z jakých součástí se zapojení skládá          |      Led pásek neopixel kruh,kabely,DHT11                           |
| realizace                                     | https://gitlab.spseplzen.cz/fremrm/fremr_r4_mood_lamp/-/blob/main/dokumentace/fotky/Cervena_lampa.jpg |
| UI                                            | https://gitlab.spseplzen.cz/fremrm/fremr_r4_mood_lamp/-/blob/main/dokumentace/design/UI.png                |
| co se mi povedlo                              |         funkčnost                        |
| co se mi nepovedlo/příště bych udělal/a jinak |            lepení                     |
| zhodnocení celé tvorby (návrh známky)         |                8/10                 |