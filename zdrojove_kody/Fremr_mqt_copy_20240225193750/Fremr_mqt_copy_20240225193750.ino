/*
 Basic ESP8266 MQTT example
 This sketch demonstrates the capabilities of the pubsub library in combination
 with the ESP8266 board/library.
 It connects to an MQTT server then:
  - publishes "hello world" to the topic "outTopic" every two seconds
  - subscribes to the topic "inTopic", printing out any messages
    it receives. NB - it assumes the received payloads are strings not binary
  - If the first character of the topic "inTopic" is an 1, switch ON the ESP Led,
    else switch it off
 It will reconnect to the server if the connection is lost using a blocking
 reconnect function. See the 'mqtt_reconnect_nonblocking' example for how to
 achieve the same result without blocking the main loop.
 To install the ESP8266 board, (using Arduino 1.6.4+):
  - Add the following 3rd party board manager under "File -> Preferences -> Additional Boards Manager URLs":
       http://arduino.esp8266.com/stable/package_esp8266com_index.json
  - Open the "Tools -> Board -> Board Manager" and click install for the ESP8266"
  - Select your ESP8266 in "Tools -> Board"
*/

#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <OneWire.h>
#include <DallasTemperature.h>
#include <Adafruit_NeoPixel.h>
#include "DHT.h"
#define typDHT11 DHT11 
#define pinDHT 4
#define INPUT_PIN 5
#define NUM_OF_LED 24
DHT mojeDHT(pinDHT, typDHT11);
const int pinCidlaDS = 13;
OneWire oneWireDS(pinCidlaDS);
DallasTemperature senzoryDS(&oneWireDS);
Adafruit_NeoPixel rgbWS = Adafruit_NeoPixel(NUM_OF_LED, INPUT_PIN, NEO_GRB + NEO_KHZ800);
// Update these with values suitable for your network.

const char* ssid = "skola";
const char* password = "1234566789";
const char* mqtt_server = "broker.hivemq.com";
unsigned long lastMsg = 0;
WiFiClient espClient;
PubSubClient client(espClient);
#define MSG_BUFFER_SIZE	(50)
char msg[MSG_BUFFER_SIZE];
int value = 0;
int red = 0;
int green = 0;
int blue = 0;
int status = 0;
void setup_wifi() {
  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  randomSeed(micros());

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}


void callback(char* topic, byte* payload, unsigned int length) {
  String a;
  for (int i = 0; i < length; i++) { 
     a += (char)payload[i];
  }
  if ((char)topic[6]=='R') {
        red = a.toInt();
        posuvnik();
        status = 0;
  } else if((char)topic[6]=='G'){ 
        green = a.toInt(); 
        posuvnik();
         status = 0;
  }

   else if((char)topic[6]=='B'){ 
       blue = a.toInt(); 
       posuvnik();
        status = 0;
   }
   else if((char)topic[6]=='X'){ 
      noc();
      status = 1;
   }
   else if((char)topic[6]=='Y'){ 
      first();
      status = 1;
   }
   
  }


void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Create a random client ID
    String clientId = "ESP8266Client-";
    clientId += String(random(0xffff), HEX);
    // Attempt to connect
    if (client.connect(clientId.c_str())) {
      Serial.println("connected");
      // Once connected, publish an announcement...
      client.publish("skupina/4hs1/fremr/ESP/welcome", "hello world");
      // ... and resubscribe
      client.subscribe("skupina/4hs1/fremr/ESP/LED");
      client.subscribe("Fremr/R");
      client.subscribe("Fremr/G");
      client.subscribe("Fremr/B");
      client.subscribe("Fremr/X");
      client.subscribe("Fremr/Y");
      client.subscribe("skupina/4hs1/fremr/ESP/LED2");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

void setup() {

  pinMode(BUILTIN_LED, OUTPUT);     // Initialize the BUILTIN_LED pin as an output
  Serial.begin(115200);
  setup_wifi();
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback); 
  pinMode(A0, INPUT);  
  pinMode(0, OUTPUT); 
  rgbWS.begin();
  rgbWS.setBrightness(255);
  mojeDHT.begin();
}


void loop() {
  if (!client.connected()) {
    reconnect();
  }
    client.loop();

  float tep = mojeDHT.readTemperature();
  float vlh = mojeDHT.readHumidity();
  unsigned long now = millis();
  if (now - lastMsg > 10000) {
      lastMsg = now;
  snprintf (msg, MSG_BUFFER_SIZE, "%.1f", tep);
  client.publish("Fremr/teplota", msg);
  snprintf (msg, MSG_BUFFER_SIZE, "%.1f", vlh);
  client.publish("Fremr/vlhkost", msg);
  }
}

void posuvnik(){ 
  for(int i = 1; i < NUM_OF_LED + 1; i++){
    rgbWS.setPixelColor(i, rgbWS.Color(red, green, blue));
    rgbWS.show();
 }}
void noc() { 
  for(int i = 1; i < NUM_OF_LED + 1; i++){
    rgbWS.setPixelColor(i, rgbWS.Color(255, 204, 203));
    rgbWS.show();
 }
}
void first() { 
for(int i = 0; i<4; i++) 
{ 
  for(int i = 1; i < NUM_OF_LED + 1; i++){
    int def = i*10;
    rgbWS.setPixelColor(i, rgbWS.Color(255, 255-def, 255));
    rgbWS.show();
 }
  delay(500);
  for(int i = 1; i < NUM_OF_LED + 1; i++){
    rgbWS.setPixelColor(i, rgbWS.Color(255, 40, 10));
    rgbWS.show();
 }
  delay(500);
 }
}

